public class Busqueda {
	
	public static int BusquedaLineal1(int vector[], int num){
		int indice = -1;
		
		for(int i=0;i<vector.length-1;i++)
			if(vector[i]==num)
				indice=i;
		
		return indice;
	}
	
	public static int BusquedaLineal2(int vector[], int num){
		int indice = -1;
		boolean salir=true;
		
		for(int i=0;i<vector.length && salir;i++)
			if(vector[i]==num){
				salir = false;
				indice=i;
			}
		
		return indice;
	}
	
	public static int BusquedaLinealX(int vector[], int num){
		int i=0;
		for(i=0;i<vector.length && vector[i]!=num;i++);
		return i;
	}
	
	public static int BusquedaLineal3(int vector[], int num){
		int indice = -1;
		boolean salir=true;
		
		for(int i=0;i<vector.length && salir;i++){
			if(vector[i]==num){
				salir = false;
				indice=i;
			}else if(vector[i]>num){
				salir = false;
			}
		}
		return indice;
	}
	
	public static int BusquedaDicotomica(int vector[], int num){
		int indice = -1,media;
		int izq=0, der=vector.length, centro=(izq+der)/2;
		boolean salir=true;
		
		do{
			if(vector[centro]==num){
				salir = false;
				indice=centro;
			}else if(vector[centro]>num && (der-izq)!=1){			
				der = centro;
				centro=(izq+der)/2;
			}else if(vector[centro]<num && (der-izq)!=1){
				izq = centro;
				centro=(izq+der)/2;
			}else if((der-izq)==1){
				salir = false;
				indice = -1;
			}
		}while(salir);		
		
		return indice;
	}

	public static void main(String[] args) {
		int vector[] = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20};
		int prueba, numPrueba;
		
		numPrueba=1;
		prueba = BusquedaDicotomica(vector,numPrueba);
		
		if(-1==prueba)
			System.out.println("El valor no existe");
		else
			System.out.println("El valor "+numPrueba+" esta en el indice "+prueba);
	}
}